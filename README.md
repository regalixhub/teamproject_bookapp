# TeamProject_BookApp

The repository contains two separate apps i.e. Front-end-> angular and Back-end-> springboot.
#### Notes:


1. We have left the Front-end-angular's port by its default i.e. 4200 and Back-end-springboot's port is 8080 (you could change this in application.properties).


2. You need also change the postgres's user credetial to your own credential in application.properties


3. If the Front-end--->angular app won't run, try to solve the issue by executing npm install in Front-end--->angular root directory.
