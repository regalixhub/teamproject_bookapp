package io.book.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.book.app.model.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{

	List<Book> findByUserId(long id);
}