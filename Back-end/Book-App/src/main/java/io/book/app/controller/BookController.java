package io.book.app.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.book.app.exception.ResourceNotFound;
import io.book.app.model.Book;
import io.book.app.service.BookService;
import io.book.app.user.ApplicationUser;
import io.book.app.user.ApplicationUserRepository;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
public class BookController {
	@Autowired
	private BookService bookService;
	
	@Autowired
	private ApplicationUserRepository userRepo;
	
	ApplicationUser userData;

	@GetMapping("/books")
	public List<Book> getAllBooks(HttpSession session){
		String name = (String) session.getAttribute("userName");
		this.userData = userRepo.findByUsername(name);
		return bookService.getAllBooks(userData.getId());
	}

	@GetMapping("/books/{bookId}")
	public Book getBookById(@PathVariable Long bookId)
	{
		return bookService.getBookById(bookId)
				.orElseThrow(() -> new ResourceNotFound("Book", "booId", bookId));
	}

	@PostMapping("/books")
	public Book addBook(@RequestBody Book book) {

		book.setUser(this.userData);
		return bookService.addBook(book);
	}

	@PutMapping("/books/{bookId}")
	public Book updateBook(@RequestBody Book book, @PathVariable Long bookId) {

		return bookService.updateBook(book);
	}

	@DeleteMapping("/books/{bookId}")
	public void deleteBook(@PathVariable Long bookId) {

		bookService.deleteBook(bookId);
	}
}