package io.book.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.book.app.model.Book;
import io.book.app.repository.BookRepository;

@Service
public class BookService {
	
	@Autowired
	public BookRepository bookRepository;
	
	public List<Book> getAllBooks(long id){
		return bookRepository.findByUserId(id);
	}

	public Optional<Book> getBookById(Long bookId) {
		return bookRepository.findById(bookId);
	}
	
	public Book addBook(Book book) {
		return bookRepository.save(book);
	}
	
	public Book updateBook(Book book) {
		return bookRepository.save(book);
	}
	
	public void deleteBook(Long bookId) {
		bookRepository.deleteById(bookId);
	}
}
