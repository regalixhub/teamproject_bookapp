package io.book.app.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import io.book.app.model.Author;

@Repository
public interface AuthorRepository extends JpaRepository<Author,Long>{

	List<Author> findByUserId(long id);
}

