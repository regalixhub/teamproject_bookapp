package io.book.app.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.book.app.model.Author;
import io.book.app.repository.AuthorRepository;

@Service
public class AuthorService {
	
	@Autowired
	private AuthorRepository authorRepository;
	
	public List<Author> getAllAuthors(long id){
		 return authorRepository.findByUserId(id);
	}
	
	public Optional<Author> getAuthorById(Long authorId){
		return authorRepository.findById(authorId);
	}
	
	public Author addAuthor(Author author) {
		 return authorRepository.save(author);
	}
	
	public String deleteAuthor(Long authorId) {
		 authorRepository.deleteById(authorId);
		 return " Successfully deleted";	
	}
	
	public Author updateAuthor(Author author) {
	 return	authorRepository.save(author);
	}

}
