package io.book.app.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import io.book.app.model.Author;
import io.book.app.service.AuthorService;
import io.book.app.user.ApplicationUser;
import io.book.app.user.ApplicationUserRepository;

@RestController
@CrossOrigin(origins = {"http://localhost:4200"})
public class AuthorController {
	
	@Autowired
	private AuthorService authorService;
	
	@Autowired
	private ApplicationUserRepository userRepo;
	
	ApplicationUser userData;
	
	@GetMapping("/authors")
	public List<Author> getAllAuthors(HttpSession session) {
		
		String name = (String) session.getAttribute("userName");
		this.userData = userRepo.findByUsername(name);
		return authorService.getAllAuthors(userData.getId());
	}
	
	@PostMapping("/authors")
	public Author addAuthor( @RequestBody Author author) {
		author.setUser(userData);
		 return authorService.addAuthor(author);
	}
	
	@GetMapping("/authors/{authorId}")
	public Optional<Author> getAuthorById(@PathVariable Long authorId) {
		return authorService.getAuthorById(authorId);
	}
	
	@PutMapping("/authors/{authorId}")
	public void updateAuthor(@PathVariable Long authorId,@RequestBody Author author)
	{
		 authorService.updateAuthor(author);
	}
	
	@DeleteMapping("/authors/{authorId}")
	public void deleteAuthor(@PathVariable Long authorId) {
		 authorService.deleteAuthor(authorId);
	}


}
